# tetris

[![Node: 8.11.2](https://img.shields.io/badge/node-8.11.2-blue.svg)]()
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

A basic tetris game.
Game ends with top out.

## Getting started

Run:
`yarn start`

Test:
`yarn test`

Hosted Logs:
`heroku login`
`heroku logs --tail --app=re-tetris`

### Curretly unsupported feaures

- Soft drop
- Waiting time on tetrimino landing
- Hold Queue
- T-Spin
- Streaks & Combos
- Choice of controller or Keyboard on start
