import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import KeyboardIcon from './images/KeyboardIcon';
import ControllerIcon from './images/ControllerIcon';

const modalRoot = document.getElementById('modal-root');

function endGame() {
  return (
    <div className="gameModal">
      <p>Game Over!</p>
    </div>
  );
}

function startGame() {
  return (
    <div className="gameModal">
      <div className="controller">
        <h2>Press Start</h2>
        <ControllerIcon />
      </div>
      <div className="keyboard">
        <h2>Press Enter</h2>
        <KeyboardIcon />
      </div>
    </div>
  );
}

class GameModal extends React.Component {
  constructor(props) {
    super(props);
    this.el = document.createElement('div');
  }

  componentDidMount() {
    modalRoot.appendChild(this.el);
  }

  componentWillUnmount() {
    modalRoot.removeChild(this.el);
  }

  render() {
    const { gameOver, isRunning } = this.props;

    if (!gameOver && isRunning) return null;
    const modalContent = gameOver ? endGame() : startGame();
    return ReactDOM.createPortal(modalContent, this.el);
  }
}

GameModal.propTypes = {
  gameOver: PropTypes.bool.isRequired,
  isRunning: PropTypes.bool.isRequired
};

export default GameModal;
