export const oLayouts = [
  {
    degrees: 0,
    layout: [{ x: 0, y: 0 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: -1, y: -1 }]
  },
  {
    degrees: 90,
    layout: [{ x: 0, y: 0 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: -1, y: -1 }]
  },
  {
    degrees: 180,
    layout: [{ x: 0, y: 0 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: -1, y: -1 }]
  },
  {
    degrees: 270,
    layout: [{ x: 0, y: 0 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: -1, y: -1 }]
  }
];

export const iLayouts = [
  {
    degrees: 0,
    layout: [{ x: 0, y: 0 }, { x: 0, y: -1 }, { x: 0, y: -2 }, { x: 0, y: -3 }]
  },
  {
    degrees: 90,
    layout: [{ x: 0, y: 0 }, { x: 1, y: 0 }, { x: 2, y: 0 }, { x: 3, y: 0 }]
  },
  {
    degrees: 180,
    layout: [{ x: 0, y: 0 }, { x: 0, y: -1 }, { x: 0, y: -2 }, { x: 0, y: -3 }]
  },
  {
    degrees: 270,
    layout: [{ x: 0, y: 0 }, { x: 1, y: 0 }, { x: 2, y: 0 }, { x: 3, y: 0 }]
  }
];

export const tLayouts = [
  {
    degrees: 0,
    layout: [{ x: 0, y: 0 }, { x: -1, y: 0 }, { x: 1, y: 0 }, { x: 0, y: -1 }]
  },
  {
    degrees: 90,
    layout: [{ x: 0, y: 0 }, { x: 0, y: -1 }, { x: 1, y: 0 }, { x: 0, y: 1 }]
  },
  {
    degrees: 180,
    layout: [{ x: 0, y: 0 }, { x: -1, y: 0 }, { x: 1, y: 0 }, { x: 0, y: 1 }]
  },
  {
    degrees: 270,
    layout: [{ x: 0, y: 0 }, { x: 0, y: 1 }, { x: 0, y: -1 }, { x: -1, y: 0 }]
  }
];

export const lLayouts = [
  {
    degrees: 0,
    layout: [{ x: 0, y: 0 }, { x: -1, y: 0 }, { x: 1, y: 0 }, { x: 1, y: -1 }]
  },
  {
    degrees: 90,
    layout: [{ x: 0, y: 0 }, { x: 0, y: -1 }, { x: 0, y: 1 }, { x: 1, y: 1 }]
  },
  {
    degrees: 180,
    layout: [{ x: 0, y: 0 }, { x: -1, y: 0 }, { x: 1, y: 0 }, { x: -1, y: 1 }]
  },
  {
    degrees: 270,
    layout: [{ x: 0, y: 0 }, { x: 1, y: 0 }, { x: 1, y: 1 }, { x: 1, y: 2 }]
  }
];

export const jLayouts = [
  {
    degrees: 0,
    layout: [{ x: 0, y: 0 }, { x: -1, y: 0 }, { x: 1, y: 0 }, { x: -1, y: -1 }]
  },
  {
    degrees: 90,
    layout: [{ x: 0, y: 0 }, { x: 0, y: 1 }, { x: 0, y: -1 }, { x: 1, y: -1 }]
  },
  {
    degrees: 180,
    layout: [{ x: 0, y: 0 }, { x: -1, y: 0 }, { x: 1, y: 0 }, { x: 1, y: 1 }]
  },
  {
    degrees: 270,
    layout: [{ x: 0, y: 0 }, { x: 0, y: -1 }, { x: 0, y: 1 }, { x: -1, y: 1 }]
  }
];

export const sLayouts = [
  {
    degrees: 0,
    layout: [{ x: 0, y: 0 }, { x: 0, y: -1 }, { x: -1, y: 0 }, { x: 1, y: -1 }]
  },
  {
    degrees: 90,
    layout: [{ x: 0, y: 0 }, { x: 0, y: 1 }, { x: -1, y: 0 }, { x: -1, y: -1 }]
  },
  {
    degrees: 180,
    layout: [{ x: 0, y: 0 }, { x: 0, y: -1 }, { x: -1, y: 0 }, { x: 1, y: -1 }]
  },
  {
    degrees: 270,
    layout: [{ x: 0, y: 0 }, { x: 0, y: 1 }, { x: -1, y: 0 }, { x: -1, y: -1 }]
  }
];

export const zLayouts = [
  {
    degrees: 0,
    layout: [{ x: 0, y: 0 }, { x: -1, y: -1 }, { x: 0, y: -1 }, { x: 1, y: 0 }]
  },
  {
    degrees: 90,
    layout: [{ x: 0, y: 0 }, { x: 0, y: 1 }, { x: -1, y: 1 }, { x: -1, y: 2 }]
  },
  {
    degrees: 180,
    layout: [{ x: 0, y: 0 }, { x: -1, y: -1 }, { x: 0, y: -1 }, { x: 1, y: 0 }]
  },
  {
    degrees: 270,
    layout: [{ x: 0, y: 0 }, { x: 0, y: -1 }, { x: -1, y: 0 }, { x: -1, y: 1 }]
  }
];
