import {
  oLayouts,
  iLayouts,
  tLayouts,
  lLayouts,
  jLayouts,
  sLayouts,
  zLayouts
} from './rotationConfig';

export const config = {
  matrix: {
    width: 10,
    height: 20
  },
  cellSize: 40,
  nextQueueLimit: 3,
  controls: {
    moveLeftKeys: ['ArrowLeft', '4'],
    moveRightKeys: ['ArrowRight', '6'],
    pauseKeys: ['Escape', 'F1', 'Enter'],
    hardDrop: ['Space', ' '],
    rotateRight: ['ArrowUp', 'x'],
    rotateLeft: ['Control', 'z']
  },
  gamePadControls: [
    { key: 0, value: ' ' },
    { key: 1, value: 'ArrowUp' },
    { key: 2, value: 'Control' },
    { key: 9, value: 'Escape' },
    { key: 14, value: 'ArrowLeft' },
    { key: 15, value: 'ArrowRight' }
  ],
  tetriminoes: {
    oTetrimino: {
      type: 'o',
      layouts: oLayouts
    },
    iTetrimino: {
      type: 'i',
      layouts: iLayouts
    },
    tTetrimino: {
      type: 't',
      layouts: tLayouts
    },
    lTetrimino: {
      type: 'l',
      layouts: lLayouts
    },
    jTetrimino: {
      type: 'j',
      layouts: jLayouts
    },
    sTetrimino: {
      type: 's',
      layouts: sLayouts
    },
    zTetrimino: {
      type: 'z',
      layouts: zLayouts
    }
  },
  tetriminoCells: [
    '0.0',
    '1.0',
    '2.0',
    '0.1',
    '1.1',
    '2.1',
    '0.2',
    '1.2',
    '2.2',
    '0.3',
    '1.3',
    '2.3'
  ]
};
