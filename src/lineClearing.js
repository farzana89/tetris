import { config } from './config/config';

const { matrix } = config;

function getLinesToClear(gridCells) {
  const linesToClear = [];
  for (let y = 0; y < matrix.height; y++) {
    const cellsContainingMino = gridCells.filter(
      line => line.loc.y === y && line.content !== ''
    );
    if (cellsContainingMino.length === matrix.width) {
      linesToClear.push(y);
    }
  }

  return linesToClear;
}

function moveMinosToReplaceClearedLines(linesToClear, gridCells) {
  const grid = gridCells;
  linesToClear.forEach(line => {
    const filteredCells = grid.filter(x => x.content !== '');
    const cells = filteredCells.filter(cell => cell.loc.y < line);

    for (let i = cells.length - 1; i >= 0; i--) {
      const cell = cells[i];
      const newRow = cell.loc.y + 1;
      const newCell = grid.find(x => x.key === `${cell.loc.x}.${newRow}`);
      if (newCell) {
        newCell.content = cell.content;
      }
      cell.content = '';
    }
  });
  return grid;
}

export function clearLines(gridCells) {
  const grid = gridCells;
  const linesToClear = getLinesToClear(gridCells);

  if (linesToClear.length > 0) {
    linesToClear.forEach(lineKey => {
      const cells = grid.filter(line => line.loc.y === lineKey);
      cells.forEach(c => {
        const cell = gridCells.find(x => x.key === c.key);
        if (cell) {
          cell.content = '';
        }
      });
    });
  }
  const clearedLines = linesToClear.length;

  return {
    linesCleared: clearedLines,
    gridCells:
      clearedLines > 0
        ? moveMinosToReplaceClearedLines(linesToClear, grid)
        : grid
  };
}
