import { config } from './config/config';

const { matrix } = config;

const rows = [...Array(matrix.height).keys()];
const cells = [...Array(matrix.width).keys()];

export function setupGrid() {
  const gridCells = [];

  rows.forEach(row => {
    cells.forEach(cell => {
      const coords = { x: cell, y: row };
      gridCells.push({
        key: `${coords.x}.${coords.y}`,
        loc: coords,
        content: '',
        isHighlighted: false
      });
    });
  });

  return gridCells;
}
