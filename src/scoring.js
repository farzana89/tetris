const scoringMap = [
  { lines: 1, points: 100 },
  { lines: 2, points: 300 },
  { lines: 3, points: 500 },
  { lines: 4, points: 800 } // tetris
];

export function calculatePoints(linesCleared, level) {
  const score = scoringMap.find(x => x.lines === linesCleared);
  if (score) {
    return score.points * level;
  }
  return 0;
}
