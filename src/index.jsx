import React from 'react';
import ReactDOM from 'react-dom';
import Game from './Game';
import { checkButtonPressed } from './gamePadHandler';
import {
  getStartingGameState,
  updateGameStateOnIteration,
  updateGameStateOnKeyPress
} from './GameEngine';
import './scss/styles.scss';

const tetrisApp = document.getElementById('tetris-app');
const gamepads = {};
let lastIterationTime = null;

window.currentGameState = getStartingGameState();

function gameRenderer(gameState) {
  ReactDOM.render(<Game {...gameState} />, tetrisApp);
}

function handleKeyPress(event) {
  window.currentGameState = updateGameStateOnKeyPress(
    window.currentGameState,
    event
  );
  gameRenderer(window.currentGameState);
}

function iterate() {
  const nextState = updateGameStateOnIteration(window.currentGameState);

  if (nextState !== window.currentGameState) {
    window.currentGameState = nextState;
    gameRenderer(window.currentGameState);
  }
}

function animate(timestamp) {
  if (!lastIterationTime) lastIterationTime = timestamp;

  const timeSinceLastIteraton = timestamp - lastIterationTime;
  const controller = navigator.getGamepads()[0];
  if (controller) {
    const pressed = checkButtonPressed(controller);
    if (pressed !== '') {
      handleKeyPress({ key: pressed });
    }
  }

  if (timeSinceLastIteraton >= window.currentGameState.gameSpeed) {
    iterate();
    lastIterationTime = timestamp;
  }
  window.requestAnimationFrame(animate);
}

function gamepadHandler(event, connecting) {
  const { gamepad } = event;

  if (connecting) {
    gamepads[gamepad.index] = gamepad;
  } else {
    delete gamepads[gamepad.index];
  }
}

// Controller listener
window.addEventListener(
  'gamepadconnected',
  e => {
    gamepadHandler(e, true);
  },
  false
);
window.addEventListener(
  'gamepaddisconnected',
  e => {
    gamepadHandler(e, false);
  },
  false
);
// This is all temporary for window listeners, won't be neccesary once animation frame is introduced

if (window.keyDownListener) {
  window.removeEventListener('keydown', window.keyDownListener);
}

animate();

window.keyDownListener = handleKeyPress;
window.addEventListener('keydown', handleKeyPress);

gameRenderer(window.currentGameState);
