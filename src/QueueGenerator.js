import { config } from './config/config';

const { nextQueueLimit, tetriminoes } = config;
const pieces = Object.values(tetriminoes);
const nextQueue = [];

function getRandomTetrimino() {
  return pieces[Math.floor(Math.random() * pieces.length)];
}

export function getNextQueue() {
  while (nextQueue.length < nextQueueLimit) {
    nextQueue.push(getRandomTetrimino());
  }
  return nextQueue;
}

export function getNextTetrimino() {
  return getNextQueue().shift();
}
