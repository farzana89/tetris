import { setupGrid } from './grid';
import { clearLines } from './lineClearing';
import { addMinoAtLoc, fillMinosAtRow } from './testSetup';

describe('Line Clearing', () => {
  it('Should return 1 line cleared for single row', () => {
    const gridCells = setupGrid();
    const grid = fillMinosAtRow(gridCells, 'o', 19);

    const result = clearLines(grid);

    expect(result.linesCleared).toEqual(1);
  });

  it('Should return 2 lines cleared for two rows', () => {
    const gridCells = setupGrid();
    let grid = fillMinosAtRow(gridCells, 'o', 18);
    grid = fillMinosAtRow(grid, 'o', 19);

    const result = clearLines(grid);

    expect(result.linesCleared).toEqual(2);
  });
});

describe('Shifting minos to replace cleared lines', () => {
  it('Should shift minos to bottom row when row beneath it is cleared', () => {
    const gridCells = setupGrid();
    let grid = fillMinosAtRow(gridCells, 'o', 19);
    grid = addMinoAtLoc(grid, 'o', { x: 5, y: 18 });

    const result = clearLines(grid);
    //  const minos = result.gridCells.filter(x => x.content !== '');

    expect(result.linesCleared).toEqual(1);
    // expect(minos[0].loc).toEqual({ x: 5, y: 19 });
  });
});
