import React from 'react';
import PropTypes from 'prop-types';
import Tetrimino from './tetriminos/Tetrimino';

function NextQueue(props) {
  const { nextQueue } = props;
  return (
    <div key="next-queue" className="next-queue">
      <h3>Next</h3>
      {nextQueue.map((tetrimino, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <Tetrimino key={index} tetrimino={tetrimino} />
      ))}
    </div>
  );
}

NextQueue.propTypes = {
  nextQueue: PropTypes.instanceOf(Array).isRequired
};

export default NextQueue;
