import { config } from './config/config';

const { gamePadControls } = config;

const buttonCache = [
  { key: 0, value: false },
  { key: 1, value: false },
  { key: 2, value: false },
  { key: 9, value: false },
  { key: 14, value: false },
  { key: 15, value: false }
];

function updateCache(key, value) {
  const cachedButton = buttonCache.find(map => map.key === key);
  if (cachedButton) {
    cachedButton.value = value;
  }
}

function cachedValue(key) {
  return buttonCache.find(x => x.key === key).value;
}

export function checkButtonPressed(controller) {
  const { buttons } = controller;
  const gamePadButtons = buttons
    .map((x, index) => ({
      key: index,
      value: x.pressed
    }))
    .filter(f => gamePadControls.some(map => map.key === f.key));

  let keyPressed = '';

  gamePadButtons.forEach(button => {
    const currValue = button.value;
    const prevValue = buttonCache.find(x => x.key === button.key).value;
    if (currValue !== prevValue) {
      if (currValue === true && cachedValue(button.key) !== true) {
        keyPressed = gamePadControls.find(x => x.key === button.key).value;
      }
      updateCache(button.key, currValue);
    }
  });

  return keyPressed;
}
