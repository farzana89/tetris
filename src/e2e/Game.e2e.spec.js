import { config } from '../config/config';
import { setupGrid } from '../grid';
import { updateGameStateOnIteration } from '../GameEngine';
import { getTetriminoCoords } from '../tetriminos/tetriminoMapper';

const { tetriminoes } = config;
const { oTetrimino } = tetriminoes;

function getStartingGameState() {
  return {
    isRunning: true,
    fallingPiece: {
      tetrimino: oTetrimino,
      position: getTetriminoCoords(oTetrimino, { x: 5, y: 1 }, 0)
    },
    gridCells: setupGrid()
  };
}

describe('Dropping tetriminos on Iteration', () => {
  it('should update tetrimino position game iteration occurs', () => {
    const updatedState = updateGameStateOnIteration(getStartingGameState());

    expect(updatedState.fallingPiece.position).toEqual([
      { x: 5, y: 2 },
      { x: 4, y: 2 },
      { x: 5, y: 1 },
      { x: 4, y: 1 }
    ]);
  });

  it('should capture minos into grid cells when tetrimino reaches bottom of grid', () => {
    const gameState = {
      isRunning: true,
      fallingPiece: {
        tetrimino: oTetrimino,
        position: getTetriminoCoords(oTetrimino, { x: 4, y: 19 }, 0)
      },
      gridCells: setupGrid()
    };
    updateGameStateOnIteration(gameState);

    const updatedCells = gameState.gridCells.filter(x => x.content !== '');

    expect(updatedCells.length).toBe(4);
  });
});
