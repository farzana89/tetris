import { config } from '../config/config';
import { setupGrid } from '../grid';
import {
  updateGameStateOnIteration,
  updateGameStateOnKeyPress
} from '../GameEngine';
import { getTetriminoCoords } from '../tetriminos/tetriminoMapper';

const { tetriminoes } = config;
const { oTetrimino } = tetriminoes;

function getStartingGameState() {
  return {
    isRunning: true,
    fallingPiece: {
      tetrimino: oTetrimino,
      position: getTetriminoCoords(oTetrimino, { x: 5, y: 1 }, 0)
    },
    gridCells: setupGrid()
  };
}

function setupGameState(position) {
  return {
    isRunning: true,
    fallingPiece: {
      tetrimino: oTetrimino,
      position: getTetriminoCoords(oTetrimino, position, 0)
    },
    gridCells: setupGrid()
  };
}

describe('Moving tetriminos', () => {
  it('should move tetrimino right for right keypress', () => {
    const updatedState = updateGameStateOnKeyPress(getStartingGameState(), {
      key: 'ArrowRight'
    });

    expect(updatedState.fallingPiece.position).toEqual([
      { x: 6, y: 1 },
      { x: 5, y: 1 },
      { x: 6, y: 0 },
      { x: 5, y: 0 }
    ]);
  });

  it('should prevent tetrimino moving right outside of the grid scope', () => {
    const gameState = setupGameState({ x: 9, y: 3 });

    const updatedState = updateGameStateOnKeyPress(gameState, {
      key: 'ArrowRight'
    });

    expect(updatedState.fallingPiece.position).toEqual([
      { x: 9, y: 3 },
      { x: 8, y: 3 },
      { x: 9, y: 2 },
      { x: 8, y: 2 }
    ]);
  });

  it('should move tetrimino left for left keypress', () => {
    const updatedState = updateGameStateOnKeyPress(getStartingGameState(), {
      key: 'ArrowLeft'
    });

    expect(updatedState.fallingPiece.position).toEqual([
      { x: 4, y: 1 },
      { x: 3, y: 1 },
      { x: 4, y: 0 },
      { x: 3, y: 0 }
    ]);
  });

  it('should prevent tetrimino moving left outside of the grid scope', () => {
    const gameState = setupGameState({ x: 1, y: 3 });
    const updatedState = updateGameStateOnKeyPress(gameState, {
      key: 'ArrowLeft'
    });

    expect(updatedState.fallingPiece.position).toEqual([
      { x: 1, y: 3 },
      { x: 0, y: 3 },
      { x: 1, y: 2 },
      { x: 0, y: 2 }
    ]);
  });
});

describe('Pausing game', () => {
  it('should not update game state when game is paused', () => {
    let gameState = setupGameState({ x: 5, y: 1 });

    gameState = updateGameStateOnKeyPress(gameState, {
      key: 'Enter'
    });

    const updatedState = updateGameStateOnIteration(gameState);

    expect(updatedState.fallingPiece).toEqual(gameState.fallingPiece);
  });
});
