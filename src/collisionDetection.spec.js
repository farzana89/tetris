import { setupGrid } from './grid';
import { collision } from './collisionDetection';
import { addMinoAtLoc } from './testSetup';

describe('O-Tetrimino Collusion detection', () => {
  it('should detect collision when tetrimino reaches bottom of matrix', () => {
    const grid = setupGrid();
    const position = [
      { x: 4, y: 19 },
      { x: 4, y: 18 },
      { x: 5, y: 18 },
      { x: 5, y: 19 }
    ];

    const collisionDetected = collision(position, grid);

    expect(collisionDetected).toBe(true);
  });

  it('should detect collision when tetrimino reaches cell containing mino', () => {
    let grid = setupGrid();

    const position = [
      { x: 4, y: 5 },
      { x: 4, y: 6 },
      { x: 5, y: 6 },
      { x: 5, y: 5 }
    ];
    grid = addMinoAtLoc(grid, 'o', { x: 5, y: 6 });

    const collisionDetected = collision(position, grid);

    expect(collisionDetected).toBe(true);
  });

  it('should not detect collision when tetrimino does not directly cross a mino beneath it', () => {
    let grid = setupGrid();
    grid = addMinoAtLoc(grid, 'o', { x: 7, y: 6 });

    const position = [
      { x: 4, y: 5 },
      { x: 4, y: 6 },
      { x: 5, y: 6 },
      { x: 5, y: 5 }
    ];
    const collisionDetected = collision(position, grid);

    expect(collisionDetected).toBe(false);
  });
});
