#!/bin/bash

set -e

echo "Deploying..."

export HEROKU_API_KEY=$HEROKU_TOKEN &&
heroku container:login &&
heroku container:push web --app=re-tetris &&
heroku container:release web --app=re-tetris && 
echo Success || echo Failed