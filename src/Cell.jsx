import React from 'react';
import PropTypes from 'prop-types';

function Cell({ children, isHighlighted }) {
  return (
    <div className={`grid-cell ${isHighlighted ? 'highlight-tile' : ''}`}>
      {children}
    </div>
  );
}

Cell.defaultProps = {
  children: null,
  isHighlighted: false
};

Cell.propTypes = {
  children: PropTypes.node,
  isHighlighted: PropTypes.bool
};

export default Cell;
