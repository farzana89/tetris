import React from 'react';
import Matrix from './Matrix';
import Guide from './Guide';
import NextQueue from './NextQueue';
import ScoreBoard from './ScoreBoard';
import GameModal from './GameModal';

function Game(props) {
  return (
    <React.Fragment>
      <Matrix {...props} />
      <NextQueue {...props} />
      <ScoreBoard {...props} />
      <Guide />
      <GameModal {...props} />
    </React.Fragment>
  );
}

export default Game;
