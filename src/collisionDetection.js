import { config } from './config/config';

const { matrix } = config;

function bottomOfMatrix(position) {
  const lastRowIndex = matrix.height - 1;
  return position.some(coord => coord.y === lastRowIndex);
}

export function collidesWithMino(position, gridCells) {
  return position.some(({ x, y }) =>
    gridCells.some(
      ({ loc, content }) => loc.y === y + 1 && loc.x === x && content !== ''
    )
  );
}

export function collision(position, gridCells) {
  return bottomOfMatrix(position) || collidesWithMino(position, gridCells);
}

export function topCollision(gridCells) {
  return gridCells.some(({ loc, content }) => loc.y === 0 && content !== '');
}
