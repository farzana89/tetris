import React from 'react';

function Guide() {
  return (
    <div className="instructions">
      <h3>Game Play</h3>
      <p>
        Pause: <span>Enter</span> | <span>Esc</span>
      </p>
      <p>
        Move left: <span>{`\u2b05`}</span>
      </p>
      <p>
        Move right: <span>{`\u2b95`}</span>
      </p>
      <p>
        Hard drop: <span>Space</span>
      </p>
    </div>
  );
}

export default Guide;
