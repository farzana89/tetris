import { config } from './config/config';
import { setupGrid } from './grid';
import { updateGameStateOnKeyPress } from './GameEngine';
import * as movement from './movement';
import { getTetriminoCoords } from './tetriminos/tetriminoMapper';

jest.mock('./movement', () => ({
  moveRight: jest.fn(() => [{ x: 5, y: 1 }]),
  moveLeft: jest.fn(() => [{ x: 4, y: 1 }]),
  hardDrop: jest.fn(() => [{ x: 5, y: 1 }]),
  rotateRight: jest.fn(() => ({ position: [{ x: 4, y: 1 }], degrees: 90 })),
  rotateLeft: jest.fn(() => ({ position: [{ x: 4, y: 1 }], degrees: 0 }))
}));

function getStartingGameState() {
  const { tetriminoes } = config;
  const { oTetrimino } = tetriminoes;
  return {
    isRunning: true,
    fallingPiece: {
      tetrimino: oTetrimino,
      position: getTetriminoCoords(oTetrimino, { x: 5, y: 1 }, 0)
    },
    gridCells: setupGrid()
  };
}

describe('Moving tetriminos', () => {
  it('should move tetrimino right for right keypress', () => {
    const moveRight = jest.spyOn(movement, 'moveRight');
    updateGameStateOnKeyPress(getStartingGameState(), {
      key: 'ArrowRight'
    });

    expect(moveRight).toBeCalled();
  });

  it('should move tetrimino left for left keypress', () => {
    const moveLeft = jest.spyOn(movement, 'moveLeft');
    updateGameStateOnKeyPress(getStartingGameState(), {
      key: 'ArrowLeft'
    });

    expect(moveLeft).toBeCalled();
  });

  it('should move tetrimino to bottom of matrix for hard drop', () => {
    const hardDrop = jest.spyOn(movement, 'hardDrop');
    updateGameStateOnKeyPress(getStartingGameState(), {
      key: ' '
    });

    expect(hardDrop).toBeCalled();
  });
});

describe('Rotating tetriminoes', () => {
  it('should rotate right for rotate right keypress', () => {
    const rotateRight = jest.spyOn(movement, 'rotateRight');
    updateGameStateOnKeyPress(getStartingGameState(), {
      key: 'ArrowUp'
    });

    expect(rotateRight).toBeCalled();
  });

  it('should rotate left for rotate left keypress', () => {
    const rotateLeft = jest.spyOn(movement, 'rotateLeft');
    updateGameStateOnKeyPress(getStartingGameState(), {
      key: 'Control'
    });

    expect(rotateLeft).toBeCalled();
  });
});

describe('Pausing game', () => {
  it('should not update game state when game is paused', () => {
    let gameState = getStartingGameState();

    expect(gameState.isRunning).toBe(true);

    gameState = updateGameStateOnKeyPress(gameState, {
      key: 'Enter'
    });

    expect(gameState.isRunning).toBe(false);
  });
});
