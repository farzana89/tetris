import { config } from './config/config';

const { matrix } = config;

export function addMinoAtLoc(gridCells, minoType, loc) {
  const key = `${loc.x}.${loc.y}`;
  const cell = gridCells.find(x => x.key === key);
  if (cell) {
    cell.content = minoType;
  }

  return gridCells;
}

export function fillMinosAtRow(gridCells, minoType, row) {
  for (let x = 0; x < matrix.width; x++) {
    addMinoAtLoc(gridCells, minoType, { x, y: row });
  }
  return gridCells;
}
