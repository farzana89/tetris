import { config } from './config/config';
import { setupGrid } from './grid';
import {
  moveDown,
  moveLeft,
  moveRight,
  hardDrop,
  rotateLeft,
  rotateRight
} from './movement';
import { getNextTetrimino, getNextQueue } from './QueueGenerator';
import { calculatePoints } from './scoring';
import { collision, topCollision } from './collisionDetection';
import { clearLines } from './lineClearing';
import { getTetriminoCoords } from './tetriminos/tetriminoMapper';

const { controls } = config;
function setupFallingTetrimino() {
  const tetrimino = getNextTetrimino();
  return {
    tetrimino,
    degrees: 0,
    position: getTetriminoCoords(tetrimino, { x: 5, y: 1 }, 0)
  };
}

function updateGrid(fallingPiece, gridCells) {
  const { position, tetrimino } = fallingPiece;
  position.forEach(mino => {
    const key = `${mino.x}.${mino.y}`;
    const cell = gridCells.find(x => x.key === key);
    if (cell) {
      cell.content = tetrimino.type;
    }
  });
}

function highlightTetriminoPath({ position }, gridCells) {
  return gridCells.map(cell => {
    const isHighlighted = position.some(
      coord => cell.loc.y > coord.y && cell.loc.x === coord.x
    );
    return Object.assign({}, cell, { isHighlighted });
  });
}

export function getStartingGameState() {
  return {
    isRunning: false,
    nextQueue: getNextQueue(),
    fallingPiece: setupFallingTetrimino(),
    gridCells: setupGrid(),
    level: 1,
    linesCleared: 0,
    score: 0,
    gameOver: false,
    gameSpeed: 500
  };
}

function checkForCollision(fallingPiece, gridCells, level) {
  let pointsGained = 0;
  let clearedLines = 0;
  let cells = gridCells;
  let falling = fallingPiece;

  const { position } = fallingPiece;
  if (collision(position, gridCells)) {
    updateGrid(fallingPiece, gridCells);
    const clearingResults = clearLines(cells);
    clearedLines = clearingResults.linesCleared;
    pointsGained = calculatePoints(clearingResults.linesCleared, level);
    cells = clearingResults.gridCells;
    falling = setupFallingTetrimino();
  }

  return {
    clearedLines,
    pointsGained,
    cells,
    fallingPiece: falling
  };
}

function checkTopOut(gridCells) {
  return topCollision(gridCells);
}

function checkLevel(gameState, clearedLines) {
  const totalLines = gameState.linesCleared + clearedLines;
  let levelNumber = gameState.level;
  let linesCarried = totalLines;
  let speed = gameState.gameSpeed;

  if (totalLines >= 10) {
    levelNumber += 1;
    linesCarried -= 10;
    speed /= 3;
  }
  return {
    levelNumber,
    linesCarried,
    gameSpeed: speed
  };
}

function updateGameState(
  gameState,
  isRunning,
  fallingPiece,
  cells,
  pointsGained,
  linesCarried,
  level,
  gameSpeed
) {
  return Object.assign({}, gameState, {
    isRunning,
    fallingPiece,
    gridCells: highlightTetriminoPath(fallingPiece, cells),
    nextQueue: getNextQueue(),
    score: gameState.score + pointsGained,
    linesCleared: linesCarried,
    level,
    gameSpeed
  });
}

function validateChanges(position, gameState, degrees, isRunning) {
  const { clearedLines, pointsGained, cells, fallingPiece } = checkForCollision(
    {
      position,
      tetrimino: gameState.fallingPiece.tetrimino,
      degrees
    },
    gameState.gridCells,
    gameState.level
  );

  const { levelNumber, linesCarried, gameSpeed } = checkLevel(
    gameState,
    clearedLines
  );

  return updateGameState(
    gameState,
    isRunning,
    fallingPiece,
    cells,
    pointsGained,
    linesCarried,
    levelNumber,
    gameSpeed
  );
}

export function updateGameStateOnIteration(gameState) {
  if (!gameState.isRunning) {
    return gameState;
  }
  const tetriminoPosition = moveDown(gameState.fallingPiece);

  const state = validateChanges(
    tetriminoPosition,
    gameState,
    gameState.fallingPiece.degrees,
    gameState.isRunning
  );
  const endGame = checkTopOut(gameState.gridCells);
  state.endGame = endGame;
  state.isRunning = !endGame;

  return state;
}

export function updateGameStateOnKeyPress(gameState, event) {
  const { key } = event;

  const { gridCells } = gameState;
  let { isRunning } = gameState;
  let tetriminoPosition = gameState.fallingPiece.position;
  let tetriminoRotation = gameState.fallingPiece.degrees;

  if (controls.pauseKeys.includes(key)) {
    isRunning = !isRunning;
  }

  if (isRunning) {
    if (controls.moveLeftKeys.includes(key)) {
      tetriminoPosition = moveLeft(tetriminoPosition, gridCells);
    }
    if (controls.moveRightKeys.includes(key)) {
      tetriminoPosition = moveRight(tetriminoPosition, gridCells);
    }
    if (controls.hardDrop.includes(key)) {
      tetriminoPosition = hardDrop(tetriminoPosition, gridCells);
    }
    if (controls.rotateRight.includes(key)) {
      const { position, degrees } = rotateRight(gameState.fallingPiece);
      tetriminoPosition = position;
      tetriminoRotation = degrees;
    }
    if (controls.rotateLeft.includes(key)) {
      const { position, degrees } = rotateLeft(gameState.fallingPiece);
      tetriminoPosition = position;
      tetriminoRotation = degrees;
    }
  }

  return validateChanges(
    tetriminoPosition,
    gameState,
    tetriminoRotation,
    isRunning
  );
}
