import { config } from './config/config';
import { setupGrid } from './grid';
import { updateGameStateOnIteration } from './GameEngine';
import { getTetriminoCoords } from './tetriminos/tetriminoMapper';
import * as movement from './movement';

const { tetriminoes } = config;
const { oTetrimino } = tetriminoes;

jest.mock('./movement', () => ({
  moveDown: jest.fn(() => [{ x: 5, y: 1 }])
}));

function setupGameState(isRunning) {
  return {
    isRunning,
    fallingPiece: {
      tetrimino: oTetrimino,
      position: getTetriminoCoords(oTetrimino, { x: 5, y: 1 }, 0)
    },
    gridCells: setupGrid()
  };
}

describe('Game changes on Iteration', () => {
  it('should move tetrimino down on iteration if game is running', () => {
    const isRunning = true;
    const moveDown = jest.spyOn(movement, 'moveDown');

    updateGameStateOnIteration(setupGameState(isRunning));

    expect(moveDown).toBeCalled();
    moveDown.mockRestore();
  });

  it('should NOT move tetrimino down on iteration if game is NOT running', () => {
    const isRunning = false;
    const moveDown = jest.spyOn(movement, 'moveDown');

    updateGameStateOnIteration(setupGameState(isRunning));

    expect(moveDown).not.toBeCalled();
    moveDown.mockRestore();
  });
});
