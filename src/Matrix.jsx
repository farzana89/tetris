import React from 'react';
import PropTypes from 'prop-types';
import Cell from './Cell';
import Mino from './tetriminos/Mino';

function Matrix(props) {
  const { gridCells, fallingPiece } = props;
  const { tetrimino, position } = fallingPiece;
  return (
    <div key="matrix" className="matrix">
      <div className="matrixGrid">
        {gridCells.map(cell => {
          const hasMino = position.find(
            coord => `${coord.x}.${coord.y}` === cell.key
          );
          if (hasMino) {
            return (
              <Cell key={cell.key}>
                <Mino type={tetrimino.type} />
              </Cell>
            );
          }
          if (cell.content !== '') {
            return (
              <Cell key={cell.key}>
                <Mino type={cell.content} />
              </Cell>
            );
          }
          return <Cell key={cell.key} isHighlighted={cell.isHighlighted} />;
        })}
      </div>
    </div>
  );
}

Matrix.propTypes = {
  gridCells: PropTypes.instanceOf(Array).isRequired,
  fallingPiece: PropTypes.instanceOf(Object).isRequired
};

export default Matrix;
