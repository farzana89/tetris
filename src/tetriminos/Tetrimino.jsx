import React from 'react';
import PropTypes from 'prop-types';
import Mino from './Mino';
import { getTetriminoCoords } from './tetriminoMapper';
import { config } from '../config/config';

const { tetriminoCells } = config;

function Tetrimino(props) {
  const { tetrimino } = props;
  const coords = getTetriminoCoords(tetrimino, { x: 1, y: 3 }, 0);
  return (
    <div className="tetriminoGrid">
      {tetriminoCells.map(key => {
        const hasMino = coords.find(coord => `${coord.x}.${coord.y}` === key);
        if (hasMino) {
          return (
            <div key={key} className="cell">
              <Mino type={tetrimino.type} />
            </div>
          );
        }
        return <div key={key} className="cell" />;
      })}
    </div>
  );
}

const tetriminoShape = PropTypes.shape({
  type: PropTypes.string.isRequired,
  layouts: PropTypes.instanceOf(Array).isRequired
});

Tetrimino.propTypes = {
  tetrimino: tetriminoShape.isRequired
};

export default Tetrimino;
