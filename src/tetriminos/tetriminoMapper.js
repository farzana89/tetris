export function getTetriminoCoords(tetrimino, position, degrees) {
  const { layouts } = tetrimino;
  const structure = layouts.find(struct => struct.degrees === degrees);
  return structure.layout.map(mino => ({
    x: position.x + mino.x,
    y: position.y + mino.y
  }));
}
