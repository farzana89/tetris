import React from 'react';
import PropTypes from 'prop-types';

function Mino({ type }) {
  return <div className={`mino ${type}`} />;
}

Mino.defaultProps = {
  type: null
};
Mino.propTypes = {
  type: PropTypes.string
};

export default Mino;
