import React from 'react';
import PropTypes from 'prop-types';

function ScoreBoard(props) {
  const { score, level } = props;
  return (
    <div key="score-board" className="score-board">
      <h3>Score Board</h3>
      <p>
        Score: <span>{score}</span>
      </p>
      <p>
        Level: <span>{level}</span>
      </p>
    </div>
  );
}

ScoreBoard.propTypes = {
  score: PropTypes.number.isRequired,
  level: PropTypes.number.isRequired
};

export default ScoreBoard;
