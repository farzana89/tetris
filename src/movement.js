import { config } from './config/config';
import { getTetriminoCoords } from './tetriminos/tetriminoMapper';
import { collidesWithMino } from './collisionDetection';

const { matrix } = config;
const cellMin = 0;
const cellMax = matrix.width - 1;
const lastRowIndex = matrix.height - 1;

function hasMino(position, gridCells, xDirection) {
  return position.some(({ x, y }) =>
    gridCells.some(
      ({ loc, content }) =>
        loc.x === x + xDirection && loc.y === y && content !== ''
    )
  );
}

function checkMatrixBoundary(position, validateBoundary) {
  return position.some(validateBoundary);
}

function moveHorizontally(position, gridCells, xDirection, validateBoundary) {
  const movePosition = [];
  const exceedsBoundary = checkMatrixBoundary(position, validateBoundary);
  const containsMino = hasMino(position, gridCells, xDirection);

  position.forEach(coord => {
    let xValue = coord.x + xDirection;
    if (exceedsBoundary || containsMino) {
      xValue = coord.x;
    }
    movePosition.push({ x: xValue, y: coord.y });
  });

  return movePosition;
}

export function moveLeft(position, gridCells) {
  const xDirection = -1;
  return moveHorizontally(
    position,
    gridCells,
    xDirection,
    coord => coord.x + xDirection < cellMin
  );
}

export function moveRight(position, gridCells) {
  const xDirection = +1;
  return moveHorizontally(
    position,
    gridCells,
    xDirection,
    coord => coord.x + xDirection > cellMax
  );
}

export function moveDown(fallingPiece) {
  const { position } = fallingPiece;

  const coords = position.map(coord => ({ x: coord.x, y: coord.y + 1 }));
  let fallingPosition = coords;

  const reachedBottom = coords.some(coord => coord.y > lastRowIndex);
  if (reachedBottom) {
    fallingPosition = position;
  }

  return fallingPosition;
}

export function rotateRight(fallingPiece) {
  const { position, tetrimino, degrees } = fallingPiece;
  const rotation = degrees + 90;
  const rotatedDegrees = rotation > 270 ? 0 : rotation;
  const coords = getTetriminoCoords(tetrimino, position[0], rotatedDegrees);
  return {
    position: coords,
    degrees: rotatedDegrees
  };
}

export function rotateLeft(fallingPiece) {
  const { position, tetrimino, degrees } = fallingPiece;
  const rotation = degrees - 90;
  const rotatedDegrees = rotation <= 0 ? 270 : rotation;
  const coords = getTetriminoCoords(tetrimino, position[0], rotatedDegrees);
  return {
    position: coords,
    degrees: rotatedDegrees
  };
}

export function hardDrop(position, gridCells) {
  const lowestY = Math.max(...position.map(({ y }) => y));

  for (let row = lowestY; row < lastRowIndex; row++) {
    const lowestPosition = position.map(({ x, y }) => ({
      x,
      y: y + (row - lowestY)
    }));

    if (collidesWithMino(lowestPosition, gridCells)) {
      return lowestPosition;
    }
  }
  return position.map(({ x, y }) => ({ x, y: y + (lastRowIndex - lowestY) }));
}
